// Taken from https://www.geeksforgeeks.org/fork-system-call/

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void
forkexample()
{
   // child process because return value zero*
   if(fork() == 0)
       printf("Hello from Child!\n");

   // parent process because return value non-zero
   else
       printf("Hello from Parent!\n");
}

int
main()
{
   forkexample();
   return 0;
}

/*
Outputs:
Hello from Parent!
Hello from Child!
*/

// According to the geeksforgeeks article, the order of Parent and Child Processes could be interchanged as the processes are run concurrently, and intial control of the program order could be given to either prior to execution, but when running this test, I only ever got Parent first, Child second.
