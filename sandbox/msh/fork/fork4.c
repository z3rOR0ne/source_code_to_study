// Taken from https://www.geeksforgeeks.org/fork-and-binary-tree/
#include <stdio.h>
#include <unistd.h>

int
main()
{
   fork(); // simple binary fork 2^1 child proccesses
   fork() && fork() || fork(); // interesting use of logical operators,
                               // see fork_diagram.jpg,
                               // essentially after the && operator only a single child process is forked,
                               // as does the || operator if that fork fails
    // essentially one can think of it as:
    //          fork that process again AND
    //          fork that process once again OR (if that fails)
    //          fork that process once again thusly only producing one fork, because if the && fork() is successful, then the || fork() is never called, and visa versa.
   fork();

   printf("forked\n");
   return 0;
}
/*
Outputs:
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
forked
*/
