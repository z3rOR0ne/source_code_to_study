// Taken from https://www.geeksforgeeks.org/fork-system-call/

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void
forkexample()
{
   int x = 1;

   if (fork() == 0)
       printf("Child has x = %d\n", ++x);
   else
       printf("Parent has x = %d\n", --x);
}

int
main()
{
   forkexample();
   return 0;
}
/*
Outputs;
Parent has x = 0
Child has x = 2
*/

// Again, the tutorial points that these outputs could be interchanged in regards to its order, but I always get this output. Apparently the new C version may have prioritized the parent process to be called first.
