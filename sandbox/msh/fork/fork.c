// Taken from https://www.geeksforgeeks.org/fork-system-call/
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int
main()
{
   // make multiple processes which run  the same
   // program after this instruction

    fork();
    fork();
    fork();

    printf("Hello world!\n");
    return 0;
}

/*
With a single call to fork() Outputs:
Hello world!
Hello world!
*/

/* With two calls to fork() Outputs:
Hello world!
Hello world!
Hello world!
Hello world!
*/

/* With three calls to fork() Outputs:
Hello world!
Hello world!
Hello world!
Hello world!
Hello world!
Hello world!
Hello world!
Hello world!
*/

/* Simple diagram that explains the forking process:

fork ();   // Line 1
fork ();   // Line 2
fork ();   // Line 3

      Parent    // The initial parent process to be forked
        |
       L1       // There will be 1 child process
    /     \     // created by line 1.
  L2      L2    // There will be 2 child processes
 /  \    /  \   //  created by line 2
L3  L3  L3  L3  // There will be 4 child processes
                // created by line 3
*/
