#include <stdio.h>
#include <stdlib.h>  // for getenv()

int main() {
    char z[100] = "I am learning C Programming language.";
    printf("My string is:\n%s\n", z);

    printf("PATH : %s\n", getenv("PATH"));
    printf("HOME: %s\n", getenv("HOME"));
    printf("USER: %s\n", getenv("USER"));

    // Always use const when declaring a pointer to a string literal
    // so it can't be overwritten
    // (declaring without const would overwrite memory, which is never a good
    // idea)
    const char *str = "1234556";
    printf("Value of str variable: %s\n", str);
    printf("Address of str variable: %p\n", &str);

    int digit = 42;
    printf("The address of digit = %p\n", &digit);
    // notice the syntax used to "unpackage" the pointer here, we point to the
    // address to get the value
    printf("The value of digit = %d\n", *(&digit));

    /* char array[100]; */
    /* printf("Enter a string\n"); */
    /* scanf("%s", array); */
    /* printf("Your string: %s\n", array); */

    /* char s[100]; */
    /* printf("Enter a string with spaces:\n"); */
    // this basic syntax can be used to replace the now depricated
    // and security flawed gets() function. NEER used gets()!!
    /* fgets(s, sizeof(s), stdin); */

    /* printf("The string you entered is: %s\n", s); */

    return 0;
}
