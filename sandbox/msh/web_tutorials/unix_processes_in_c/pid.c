#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
    int id = fork();
    if (id == 0) {
        sleep(1);
    }
    printf("Current ID: %d, parent ID: %d\n", getpid(), getppid());

    // if statement not really necessary here, just for demo purposes
    /* if (id != 0) { */
        /* wait(NULL); */
    /* } */
    int res = wait(NULL);
    if (res == -1) {
       printf("No children to wait for\n");
    } else {
       printf("%d finished execution\n", res);
    }
    return 0;
}
