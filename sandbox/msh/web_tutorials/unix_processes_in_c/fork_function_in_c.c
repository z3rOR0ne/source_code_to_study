#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>  // Linux only

int main(int argc, char* argv[]) {
    int id = fork();
    if (id != 0) {
        // if NOT child process (Parent process)
        // capable of creating odd number of processes
       fork();
    }
    printf("Hello world\n");
    /* printf("Hello world from id: %d \n", id); */
    /* if (id == 0) { */
       /* printf("Hello from child process\n"); */
    /* } else { */
       /* printf("Hello from the main process\n"); */
    /* } */
    return 0;
}

