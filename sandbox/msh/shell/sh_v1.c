#include <limits.h>  // for HOST_NAME_MAX
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#define LSH_TOK_BUFSIZE 64
#define LSH_TOK_DELIM " \t\r\n\a"

// reads stdin and returns its value (a string, which in this case is meant to
// be a command)
char *lsh_read_line(void) {
    char *line = NULL;
    ssize_t bufsize = 0; /* have getline allocate a buffer for us */

    /* https://linuxhint.com/getline-function-c/ */
    /* size_t getline (char **string, size_t *n, FILE *stream); */
    // if the return value of getline() returns a non-value (NULL)...
    if (getline(&line, &bufsize, stdin) == -1) {
        // feof stands for FILE *stream EOF, it looks for the EOF, in this case
        // from stdin
        if (feof(stdin)) {      // if we've reached the EOF, exit with a SUCCESS
                                // status
            exit(EXIT_SUCCESS); /* We received an EOF */
        } else {  // if EOF is not reached by stdin, print an error and exit
                  // with a FAILURE status
            perror("readline");
            exit(EXIT_FAILURE);
        }
    }
    // return the output of the stdin stream
    return line;
}

char **lsh_split_line(char *line) {
    int bufsize = LSH_TOK_BUFSIZE, position = 0;
    char **tokens = malloc(bufsize * sizeof(char *));
    /* char *token; */
    char *token = NULL;

    if (!tokens) {
        fprintf(stderr, "lsh: allocation error\n");
        exit(EXIT_FAILURE);
    }

    /* The C library function char *strtok(char *str, const char *delim)
     * breaks string str into a series of tokens using the delimiter delim. */
    token = strtok(line, LSH_TOK_DELIM);
    while (token != NULL) {
        tokens[position] = token;
        position++;

        if (position >= bufsize) {
            bufsize += LSH_TOK_BUFSIZE;
            tokens = realloc(tokens, bufsize * sizeof(char *));
            if (!tokens) {
                fprintf(stderr, "lsh: allocation error\n");
                exit(EXIT_FAILURE);
            }
        }
        // in each subsequent call to strtok, the strtok(char *str) value must
        // be NULL
        token = strtok(NULL, LSH_TOK_DELIM);
    }
    tokens[position] = NULL;
    return tokens;
}

int lsh_launch(char **args) {
    pid_t pid, wpid;
    int status;
    pid = fork();
    if (pid == 0) {
        // Child Process
        if (execvp(args[0], args) == -1) {
            perror("lsh");
        }
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        // Error forking
        perror("lsh");
    } else {
        // Parent Process
        do {
            wpid = waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }

    return 1;
}

/*
 * Function Declarations for builtin shell commands:
 */

int lsh_cd(char **args);
int lsh_help(char **args);
int lsh_exit(char **args);

/*
 * List of builtin commands, followed by their corresponding functions.
 */

char *builtin_str[] = {"cd", "help", "exit"};

int (*builtin_func[])(char **) = {&lsh_cd, &lsh_help, &lsh_exit};

int lsh_num_builtins() { return sizeof(builtin_str) / sizeof(char *); }

/*
 * Builtin function implementations.
 */

int lsh_cd(char **args) {
    if (args[1] == NULL) {
        // Changed default behavior so it acts more like standard cd command, cd
        // now simply goes to $HOME directory
        char *home = getenv("HOME");
        chdir(home);
        /* fprintf(stderr, "lsh: expected argument to \"cd\"\n"); */
    } else {
        if (chdir(args[1]) != 0) {
            perror("lsh");
        }
    }
    return 1;
}

int lsh_help(char **args) {
    int i;
    printf("Stephen Brennan's LSH\n");
    printf("Type program names and arguments, and hit enter.\n");
    printf("The following are builtin:\n");

    for (i = 0; i < lsh_num_builtins(); i++) {
        printf("  %s\n", builtin_str[i]);
    }

    printf("Use the man command for information on other programs.\n");
    return 1;
}

int lsh_exit(char **args) { return 0; }

int lsh_execute(char **args) {
    int i;

    if (args[0] == NULL) {
        // An empty commmand was entered.
        return 1;
    }

    for (i = 0; i < lsh_num_builtins(); i++) {
        if (strcmp(args[0], builtin_str[i]) == 0) {
            return (builtin_func[i](args));
        }
    }

    return lsh_launch(args);
}

void lsh_loop(void) {
    char *line;
    char **args;
    int status;

    // https://www.systutorials.com/how-to-get-the-hostname-of-the-node-in-c/
    char hostname[HOST_NAME_MAX + 1];
    gethostname(hostname, HOST_NAME_MAX + 1);

    char *username = getenv("USER");
    do {
        printf("[%s@%s]$\n└─> ", username, hostname);
        line = lsh_read_line();
        args = lsh_split_line(line);
        status = lsh_execute(args);

        free(line);
        free(args);
    } while (status);
}

int pipeping() { /* int argc, char* argv[]) { */
    int fd[2];
    if (pipe(fd) == -1) return 1;

    int pid1 = fork();
    if (pid1 < 0) return 2;

    if (pid1 == 0) {
        // Child process 1 (ping)
        // fd[1] is the "write" end of the pipe, writing to standard output
        dup2(fd[1], STDOUT_FILENO);
        close(fd[0]);
        close(fd[1]);
        execlp("ping", "ping", "-c", "5", "google.com", NULL);
    }

    int pid2 = fork();
    if (pid2 < 0) return 3;

    if (pid2 == 0) {
        // Child proccess 2 (grep)
        dup2(fd[0], STDIN_FILENO);
        close(fd[0]);
        close(fd[1]);
        execlp("grep", "grep", "rtt", NULL);
    }

    close(fd[0]);
    close(fd[1]);

    waitpid(pid1, NULL, 0);
    waitpid(pid2, NULL, 0);
    return 0;
}

/* int main(void) { lsh_loop(); } */
int main(void) { pipeping(); }
