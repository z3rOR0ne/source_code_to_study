#include <stdio.h>
#include <stdlib.h>

int main() {
    char *username = getenv("USER");
    char *home = getenv("HOME");
    printf("User is: %s\n", username);
    printf("Home directory is: %s\n", home);
}
