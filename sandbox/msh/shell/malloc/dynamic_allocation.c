/* https://overiq.com/c-programming-101/the-malloc-function-in-c/ */

#include <stdio.h>
#include <stdlib.h>

int main() {
    float *p, sum = 0;
    int i, n;

    printf("Enter the number of students: ");
    scanf("%d", &n);

    // allocate memory to store n variables of type float
    p = (float*)malloc(n*sizeof(float));

    // if dynamic allocation failed exit the c-program
    if(p == NULL) {
        printf("Memory allocation failed");
        exit(1); // exit the program
    }

    // ask the student to enter marks
    for(i = 0; i < n; i++) {
        printf("Enter marks for %d students: ", i+1);
        scanf("%f", p + i);
    }

    // calculate sum
    for(i = 0; i < n; i++) {
        sum += *(p+i);
    }

    printf("\nAverage marks = %.2f\n", sum / n);

    free(p);
    // signal to operating sysmte program ran fine
    return 0;
}
