/* https://www.geeksforgeeks.org/sizeof-operator-c/ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 64

int main() {
    printf("Size of char: %lu\n", sizeof(char));
    printf("Size of char pointer: %lu\n", sizeof(char *));
    printf("Size of int: %lu\n", sizeof(int));
    printf("Size of float: %lu\n", sizeof(float));
    printf("Size of double: %lu\n", sizeof(double));

    int bufsize = BUF_SIZE;
    printf("BUF_SIZE is: %d\n", bufsize);

    /* https://stackoverflow.com/questions/1963780/when-should-i-use-malloc-in-c-and-when-dont-i
     */
    char *some_memory;
    size_t size_to_allocate = bufsize;
    some_memory = (char *)malloc(size_to_allocate);
    sprintf(some_memory, "Hello World");
    printf("first hello: %s\n", some_memory);
    some_memory[0] = 'h';
    printf("second hello: %s\n", some_memory);
    free(some_memory);
}
