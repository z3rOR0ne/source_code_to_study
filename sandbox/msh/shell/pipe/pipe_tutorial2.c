/* https://www.youtube.com/watch?v=6xbLgZpOBi8 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAX_CMD_LENGTH 100
#define MAX_NUM_PARAMS 100

int parsecmd(char *cmd, char** params) { //split cmd into array of params
    int i,n=-1;
    for(i=0; i<MAX_NUM_PARAMS; i++) {
        params[i] = strsep(&cmd, " ");
        /* printf("%s\n", params[i]); */
        n++;
        if(params[i] == NULL) break;
    }
    return(n);
};

void print_array(char **strings, size_t nptrs) {

    for (size_t i = 0; i < nptrs; i++) {
        printf("%s, ", strings[i]);
    }
    putchar('\n');
}

int main(int argc, char* argv[]) {
    char cmd[MAX_CMD_LENGTH+1];
    char * params[MAX_NUM_PARAMS+1];
    char * strings[] = { "Hello", "Zerotom", "new" };
    fgets(cmd, sizeof(cmd), stdin); //read command, ctrl+C exit
    print_array(strings, sizeof strings/sizeof * strings);
    int j=parsecmd(cmd, params); //split cmd into array of params
    /* execlp(params[0], params[0], NULL); */
    execvp(params[0], params);
    /* int i; */
    /* for (i = 0; i < j; i++) { */
        /* printf("%s\n", params[i]); */
    /* } */

    return 0;

    /* int fd[2]; */
    /* if (pipe(fd) == -1) return 1; */

    /* int pid1 = fork(); */
    /* if (pid1 < 0) return 2; */

    /* if (pid1 == 0) { */
        /* // Child process 1 (ping) */
        /* // fd[1] is the "write" end of the pipe, writing to standard output */
        /* dup2(fd[1], STDOUT_FILENO); */
        /* close(fd[0]); */
        /* close(fd[1]); */
        /* execlp("ping", "ping", "-c", "5", "google.com", NULL); */
    /* } */

    /* int pid2 = fork(); */
    /* if (pid2 < 0) return 3; */

    /* if (pid2 == 0) { */
        /* // Child proccess 2 (grep) */
        /* dup2(fd[0], STDIN_FILENO); */
        /* close(fd[0]); */
        /* close(fd[1]); */
        /* execlp("grep", "grep", "rtt", NULL); */
    /* } */

    /* close(fd[0]); */
    /* close(fd[1]); */

    /* waitpid(pid1, NULL, 0); */
    /* waitpid(pid2, NULL, 0); */
    /* return 0; */
}
