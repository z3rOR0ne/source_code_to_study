/* https://stackoverflow.com/questions/14300880/how-to-run-a-command-using-pipe
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_FILE_LENGTH 100

int main() {
    // int nbBytes = 0; //stream length
    int pfd_1[2];  // file descriptor
    // char buffer[MAX_FILE_LENGTH];
    char* arg[MAX_FILE_LENGTH];
    pid_t processPid;

    // Create a pipe

    if (pipe(pfd_1) == -1) {
        printf("Error in creating pipe");
        return 0;
    }

    // Create a child
    processPid = fork();

    if (processPid == -1) {
        printf("Erro in fork");
        exit(1);
    } else if (processPid == 0)  // Child
    {
        // redirect read end file descriptor to standard input
        dup2(pfd_1[0], 0);
        // Close the write end
        if (close(pfd_1[1]) == -1) {
            printf("Error in closing the write end file descriptor");
            exit(1);
        }
        arg[0] = "wc";
        // arg[1] = "-l";
        arg[1] = '\0';

        if (execvp(arg[0], arg) == -1) {
            printf("Error in executing ls");
        }

    } else  // Parent {
        // redirect standard output to the file descriptor
        dup2(pfd_1[1], 1);
    // Close the read end
    if (close(pfd_1[0]) == -1) {
        printf("Error in closing the read end from parent");
        exit(1);
    }
    // Command
    arg[0] = "ls";
    arg[1] = "/proc/1/status";
    arg[2] = '\0';

    if (execvp(arg[0], arg) == -1) {
        printf("Error in executing ls");
    }
}
