#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>

/* int main() { */
    /* char* argv[10]; */
    /* argv[0] = "/usr/sbin/ls"; */
    /* fork(); */
    /* execvp(argv[0], argv); */
    /* [> while (1) { <] */
       /* [> printf(">\n"); <] */
       /* [> break; <] */
    /* [> } <] */
/* } */

int  main(void)
{
pid_t cpid;
int   status = 0;

cpid = fork();
  if (cpid == 0)
  {
   /* char *cmd[] = { "ls", "-l", (char *)0 }; */
   char *cmd[] = { "ls", "-l", NULL };
   int ret = execv ("/bin/ls", cmd);
  }
wait(&status);
  if(status < 0)
     perror("Abnormal exit of program ls\n");
  else
     printf("Exit Status of ls is %d\n",status);
}
