/* https://stackoverflow.com/questions/33912024/shell-program-with-pipes-in-c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAX_CMD_LENGTH 100

#define MAX_NUM_PARAMS 10

int parsecmd(char *cmd, char **params) {  // split cmd into array of params
    int i, n = -1;
    for (i = 0; i < MAX_NUM_PARAMS; i++) {
        params[i] = strsep(&cmd, " ");
        n++;
        if (params[i] == NULL) break;
    }
    return (n);
};

int executecmd(char **params) {
    pid_t pid = fork();  // create child process that is a clone of the parent

    if (pid == -1) {  // error
        char *error = strerror(errno);
        printf("error fork!!\n");
        return 1;
    } else if (pid == 0) {          // child process
        execvp(params[0], params);  // exec cmd
        char *error = strerror(errno);
        printf("unknown command\n");
        return 0;
    } else {  // parent process
        int childstatus;
        waitpid(pid, &childstatus,
                0);  // wait for child process to finish before exiting
        return 1;
    }
};

int execpipe(char **argv1, char **argv2) {
    int fds[2];  // an array that will hold two file descriptors
    pipe(fds);   // populates fds with two file descriptors

    pid_t pid = fork();  // create child process that is a clone of the parent

    if (pid == -1) {  // error
        char *error = strerror(errno);
        printf("error fork!!\n");
        return 1;
    }
    if (pid == 0) {       // child process
        close(fds[1]);    // file descriptor unused in child
        dup2(fds[0], 0);  // fds[0] reads the end of the pipe and donates its
                          // data to the file descriptor 0
        close(fds[0]);
        // file descriptor no longer needed in child since stdin is a copy
        execvp(argv2[0], argv2);
        // run command AFTER pipe character in userinput

        // simple error handling
        char *error = strerror(errno);
        printf("unknown command\n");
        return 0;
    } else {              // parent process
        close(fds[0]);    // file descriptor unused in parent
        dup2(fds[1], 1);  // STDIN
        close(fds[1]);
        execvp(argv1[0], argv1);
        // run command BEFORE pipe character in userinput

        int childstatus;
        waitpid(pid, &childstatus, 0);
        // wait for child process to finish before exiting

        // simple error handling
        char *error = strerror(errno);
        printf("unknown command\n");
        return 0;
    }
};

int main() {
    char cmd[MAX_CMD_LENGTH + 1];
    char *params[MAX_NUM_PARAMS + 1];
    char *argv1[MAX_NUM_PARAMS + 1] = {0};
    char *argv2[MAX_NUM_PARAMS + 1] = {0};
    int k, y, x;
    int f = 1;
    while (1) {
        printf("sh>$ ");  // prompt
        if (fgets(cmd, sizeof(cmd), stdin) == NULL)
            break;                           // read command, ctrl+D exit
        if (cmd[strlen(cmd) - 1] == '\n') {  // remove newline char
            cmd[strlen(cmd) - 1] = '\0';
        }
        int j = parsecmd(cmd, params);  // split cmd into array of params
        if (strcmp(params[0], "exit") == 0) break;  // exit
        for (k = 0; k < j; k++) {
            if (strcmp(params[k], "|") == 0) {  // if pipe is found
                f = 0;  // set f variable (previously 1) to 0
                y = k;  // and whatever y is (undefined) is equal to k (the
                        // count of words up until the "|" was found)
                break;
            }
        }
        if (f == 0) {                  // if pipe was found...
            for (x = 0; x < k; x++) {  // loop through the words
                argv1[x] = params[x];  // and the argument arrays is given a set
                                       // of parameters from our inputted cmd
            }
            int z = 0;
            for (x = k + 1; x < j;
                 x++) {  // one command ahead of k (k + 1) is equal to x, which
                         // is compared to the length of the params array(j),
                         // and looped over
                argv2[z] = params[x];  // whatever is read after the pipe is
                                       // assigned to argv2[z]
                z++;
            }
            if (execpipe(argv1, argv2) == 0) break;
            // and we executepipe() function (exits after executing)
        } else if (f == 1) {  // if pipe was not found
            // simply execute the command (does not exit shell after executing)
            if (executecmd(params) == 0) break;
            // this exits after executing:
            /* execvp(params[0], params);  // exec cmd */
        }
    }  // end while
    return 0;
}
