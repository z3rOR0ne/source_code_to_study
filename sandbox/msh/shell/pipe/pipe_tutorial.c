/* https://www.youtube.com/watch?v=6xbLgZpOBi8 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char* argv[]) {

    /* cast to void to silence compiler warnings */
    (void)argc;
    (void)argv;

    int fd[2];
    if (pipe(fd) == -1) return 1;

    /* int pid1 = fork(); */
    pid_t pid1 = fork();
    if (pid1 < 0) return 2;

    if (pid1 == 0) {
        // Child process 1 (ping)
        // fd[1] is the "write" end of the pipe, writing to standard output
        dup2(fd[1], STDOUT_FILENO);
        close(fd[0]);
        close(fd[1]);
        execlp("ping", "ping", "-c", "5", "google.com", NULL);
    }

    int pid2 = fork();
    if (pid2 < 0) return 3;

    if (pid2 == 0) {
        // Child proccess 2 (grep)
        dup2(fd[0], STDIN_FILENO);
        close(fd[0]);
        close(fd[1]);
        execlp("grep", "grep", "rtt", NULL);
    }

    close(fd[0]);
    close(fd[1]);

    waitpid(pid1, NULL, 0);
    waitpid(pid2, NULL, 0);
    return 0;
}
