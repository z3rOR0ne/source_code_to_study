/* https://stackoverflow.com/questions/69292956/why-are-pipe-operations-not-working-in-my-shell */

/* #include <stdio.h> */
/* #include <stdlib.h> */
#include <unistd.h> // brings in STDIN_FILENO, NULL
#include <sys/wait.h> // brings in STDIN_FILENO, NULL

#define MAX_NO_ARGS  100

struct command {
    char *comm;
    char *args[MAX_NO_ARGS+2];
    int no_args;
    int order;
};

int execute_command(char *command, int argc, char *args[], int fd_in, int fd_out)
{
    int pid;
    pid = fork();
    // Child process
    if (pid == 0) {
        if (fd_in) {
            // https://linuxhint.com/dup2_system_call_c/
            dup2(fd_in, STDIN_FILENO);
            close(fd_out);
            execvp(command, args);
        }
        if (fd_out != 1) {
            dup2(fd_out, STDOUT_FILENO);
            close(fd_out);
            execvp(command, args);
        }
        if (fd_in == fd_out == -1) {
            execvp(command, args);
        }
    }
    waitpid(pid, NULL, 0);
    return 0;
}

// go time, this is what you have to really understand
void execute_commands(struct command mycommands[], int no_commands)
{
    // if there is no pipe for commands to go through, i.e. there is only one command, then simply execute it
    if (no_commands == 1) {
        execute_command(mycommands[0].comm, mycommands[0].no_args, mycommands[0].args, -1, -1);
    } else {
        int i = 0; // j = 0;
        int fd[no_commands-1][2];
        for (i = 0; i < no_commands; i++) {
            pipe(fd[i]);

        }
        for (i = 0; i < no_commands; i++) {
            if (i == 0) {
                execute_command(mycommands[i].comm, mycommands[i].no_args, mycommands[i].args, -1, fd[i][1]);
            } else if (i == no_commands -1 ) {
                execute_command(mycommands[i].comm, mycommands[i].no_args, mycommands[i].args, fd[i-1][0], -1);
            } else {
                execute_command(mycommands[i].comm, mycommands[i].no_args, mycommands[i].args, fd[i-1][0], fd[i][1]);
            }
        }

        for (i = 0; i < no_commands-1; i++) {
            close(fd[i][0]);
        }
    }
    return;
}


