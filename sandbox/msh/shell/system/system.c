/* https://www.tutorialspoint.com/c_standard_library/c_function_system.htm */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char command[50];

    strcpy(command, "ls -l");
    system(command);

    return(0);
}
