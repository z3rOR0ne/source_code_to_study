#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LSH_TOK_BUFSIZE 64
#define LSH_TOK_DELIM " \t\r\n\a"

char **lsh_split_line(char *line) {
    int bufsize = LSH_TOK_BUFSIZE, position = 0;
    char **tokens = malloc(bufsize * sizeof(char *));
    char *token;
    /* char *token = NULL; */

    if (!tokens) {
        fprintf(stderr, "lsh: allocation error\n");
        exit(EXIT_FAILURE);
    }

    /* The C library function char *strtok(char *str, const char *delim)
     * breaks string str into a series of tokens using the delimiter delim. */
    token = strtok(line, LSH_TOK_DELIM);
    while (token != NULL) {
        tokens[position] = token;
        position++;

        if (position >= bufsize) {
            bufsize += LSH_TOK_BUFSIZE;
            tokens = realloc(tokens, bufsize * sizeof(char *));
            if (!tokens) {
                fprintf(stderr, "lsh: allocation error\n");
                exit(EXIT_FAILURE);
            }
        }
        // in each subsequent call to strtok, the strtok(char *str) value must
        // be NULL
        token = strtok(NULL, LSH_TOK_DELIM);
    }
    tokens[position] = NULL;
    printf("%c\n", **tokens);
    return tokens;
}

int main() {
    // playing around...
    /* char line[10] = {'h','i',' ','t','h','e','r','e','!'}; */
    /* printf("%s\n", line); */

    char *line = 'ls';
    lsh_split_line(line);
}
