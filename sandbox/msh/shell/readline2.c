#include <stdio.h>
#include <stdlib.h>

char *lsh_read_line(void) {
    char *line = NULL;
    ssize_t bufsize = 0; /* have getline allocate a buffer for us */

    if (getline(&line, &bufsize, stdin) == -1) {
        if (feof(stdin)) {
            exit(EXIT_SUCCESS); /* We received an EOF */
        } else {
            perror("readline");
            exit(EXIT_FAILURE);
        }
    }
    return line;
}

/* int */
/* main() */
/* { */
/* lsh_read_line(); */
/* } */
