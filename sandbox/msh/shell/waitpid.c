// https://www.delftstack.com/howto/c/waitpid-in-c/

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

pid_t spawnChild(const char* program, char** arg_list) {
    pid_t ch_pid = fork();
    if (ch_pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (ch_pid > 0) {
        printf("spawn child with pid -%d\n", ch_pid);
        return ch_pid;
    } else {
        /* https://www.delftstack.com/howto/c/execvp-in-c/ */
        execvp(program, arg_list);
        perror("execve");
        exit(EXIT_FAILURE);
    }
}

int main(void) {
    /* Tutorials show const char* args[],
     * but const causes problems with execvp as the args[] needs to be mutable.
     */
    char* args[] = {"ls", "--color=auto", "-liash", NULL};

    pid_t child;
    int wstatus;

    child = spawnChild("ls", args);

    if (waitpid(child, &wstatus, WUNTRACED | WCONTINUED) == -1) {
        perror("waitpid");
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

