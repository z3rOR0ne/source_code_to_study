#include <limits.h>  // for HOST_NAME_MAX
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#define LSH_TOK_BUFSIZE 64
#define LSH_TOK_DELIM " |\t\r\n\a"  // notice the space at the beginning here, indicating a space
                 // as well

// reads stdin and returns its value (a string, which in this case is meant to
// be a command)
char *lsh_read_line(void) {
    char *line = NULL;
    ssize_t bufsize = 0; /* have getline allocate a buffer for us */

    /* https://linuxhint.com/getline-function-c/ */
    /* size_t getline (char **string, size_t *n, FILE *stream); */
    // if the return value of getline() returns a non-value (NULL)...
    if (getline(&line, &bufsize, stdin) == -1) {
        // feof stands for FILE *stream EOF, it looks for the EOF, in this case
        // from stdin
        if (feof(stdin)) {      /* if we've reached the EOF, exit with a SUCCESS
                                   status */
            exit(EXIT_SUCCESS); /* We received an EOF */
        } else {  // if EOF is not reached by stdin, print an error and exit
                  // with a FAILURE status
            perror("readline");
            exit(EXIT_FAILURE);
        }
    }
    // return the output of the stdin stream
    return line;
}

char **lsh_split_line(char *line) {
    int bufsize = LSH_TOK_BUFSIZE, position = 0;
    // allocates 64 bytes multiplied by the sizeof(char *, which is 8, making it
    // 512 allocated bytes...)
    char **tokens = malloc(bufsize * sizeof(char *));
    /* char *token; */
    char *token = NULL;

    if (!tokens) {
        fprintf(stderr, "lsh: allocation error\n");
        exit(EXIT_FAILURE);
    }

    /* The C library function char *strtok(char *str, const char *delim)
     * breaks string str into a series of tokens using the delimiter delim(a
     * string of delimiters). */
    token = strtok(line, LSH_TOK_DELIM);
    while (token != NULL) {
        tokens[position] = token;
        /* printf("our token is: %s\n", token); // indeed returns the tokens of
         * the command */
        position++;

        // returns correctly
        /* if (strchr(token, '|') != NULL) { */
            /* printf("pipe character detected!\n"); */
        /* } */

        /* Also returns correctly, no spaces are needed even in zsh for a pipe, so I guess it doesn't really matter,
         * but now we'll have to determine what happens before and after  the pipe symbol is entered*/
        /* if (strchr(LSH_TOK_DELIM, '|') != NULL) { */
            /* printf("pipe delimiter detected!\n"); */
        /* } */

        if (position >= bufsize) {
            bufsize += LSH_TOK_BUFSIZE;
            tokens = realloc(tokens, bufsize * sizeof(char *));
            if (!tokens) {
                fprintf(stderr, "lsh: allocation error\n");
                exit(EXIT_FAILURE);
            }
        }
        // in each subsequent call to strtok, the strtok(char *str) value must
        // be NULL
        token = strtok(NULL, LSH_TOK_DELIM);
    }
    tokens[position] = NULL;
    return tokens;
}

// takes the arguments from lsh_execute and executes them using execvp()
// here is where we will probably have to pipe() and fork() and dup2() processes
// to utilize the | redirection operator
int lsh_launch(char **args, int fd_in, int fd_out) {
    pid_t pid, wpid;
    int status;
    pid = fork();
    if (pid == 0) {

        // taken from pipe_loop.c
        if (fd_in) {
            dup2(fd_in, STDIN_FILENO);
            close(fd_out);
            execvp(args[0], args);
        }
        if (fd_out !=1) {
           dup2(fd_out, STDOUT_FILENO);
           close(fd_out);
           execvp(args[0], args);
        }
        if (fd_in == fd_out == -1) {
           execvp(args[0], args);
        }
        // end of taken from pipe_loop.c

        // Child Process
        // here the if execvp() statement is checked against, but
        // remember, it is also EXECUTED!

        if (execvp(args[0], args) == -1) {
            perror("lsh");
        }
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        // Error forking
        perror("lsh");
    } else {
        // Parent Process
        do {
            wpid = waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }

    return 1;
}

/*
 * Function Declarations for builtin shell commands:
 */

int lsh_cd(char **args);
int lsh_help(char **args);
int lsh_exit(char **args);

/*
 * List of builtin commands, followed by their corresponding functions.
 */

// creates an array of strings recognized as builtin commands
char *builtin_str[] = {"cd", "help", "exit"};

// creates an array of built in functions
int (*builtin_func[])(char **) = {&lsh_cd, &lsh_help, &lsh_exit};

// returns the length of the builtin_str array
int lsh_num_builtins() { return sizeof(builtin_str) / sizeof(char *); }

/*
 * Builtin function implementations.
 */

int lsh_cd(char **args) {
    if (args[1] == NULL) {
        // Changed default behavior so it acts more like standard cd command, cd
        // now simply goes to $HOME directory
        char *home = getenv("HOME");
        chdir(home);
        /* fprintf(stderr, "lsh: expected argument to \"cd\"\n"); */
    } else {
        if (chdir(args[1]) != 0) {
            perror("lsh");
        }
    }
    return 1;
}

int lsh_help(char **args) {
    int i;
    printf("Stephen Brennan's LSH\n");
    printf("Type program names and arguments, and hit enter.\n");
    printf("The following are builtin:\n");

    for (i = 0; i < lsh_num_builtins(); i++) {
        printf("  %s\n", builtin_str[i]);
    }

    printf("Use the man command for information on other programs.\n");
    return 1;
}

int lsh_exit(char **args) { return 0; }

int lsh_execute(char **args, int fd_in, int fd_out) {
    int i;

    if (args[0] == NULL) {
        // An empty commmand was entered.
        return 1;
    }

    for (i = 0; i < lsh_num_builtins(); i++) {
        // if the passed argument contains any of our commands from the
        // builtin_str array, execute that using our builtin_func()
        if (strcmp(args[0], builtin_str[i]) == 0) {
            return (builtin_func[i](args));
        }
    }
    // otherwise simply launch our argument using the lsh_launch() function
    return lsh_launch(args, fd_in, fd_out);
}

void lsh_loop(void) {
    int fd_in, fd_out;
    char *line;
    char **args;
    int status;

    // https://www.systutorials.com/how-to-get-the-hostname-of-the-node-in-c/
    char hostname[HOST_NAME_MAX + 1];
    gethostname(hostname, HOST_NAME_MAX + 1);

    // grab the $USER environment variable
    char *username = getenv("USER");

    do {
        // Simply prints the PROMPT
        printf("[%s@%s]$\n└─> ", username, hostname);
        // simply gets the input and returns it into the value of line
        // (established at the beginning of this function)
        line = lsh_read_line();
        // splits the line into ...
        args = lsh_split_line(line);
        // executes the arguments
        status = lsh_execute(args, fd_in, fd_out);

        free(line);
        free(args);
    } while (status);
}

int main(void) { lsh_loop(); }
