// Taken from https://www.includehelp.com/c/process-identification-pid_t-data-type.aspx
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int
main(void)
{
   pid_t process_id;
   pid_t p_process_id;

   process_id = getpid();
   p_process_id = getppid();

   printf("The process id: %d\n", process_id);
   printf("The process id of parent function: %d\n", p_process_id);

   return 0;
}

/*
Outputs:
The process id: 28004
The process id of parent function: 17605
*/

// Interesting behavior:
// the process id will change its address, but the parent process is always allocated to the same address (unless recompiled of course)
