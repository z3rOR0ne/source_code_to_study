// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation
//
#include <stdio.h>

int multiply(int *x, int *y) {
    int z;
    z = (*x) * (*y);
    return z;
}

int main() {
    int x = 3, y = 5;
    int product = multiply(&x, &y);
    printf("Product = %d\n", product);
    /* Output:
    Product = 15
    */
}

/* NOTE:
Since we created stored values in a new location, it costs us memory. Wouldn't
it be better if we could perform the same task without wasting space?

Call by reference helps us achieve this. We pass the address or reference of the
variables to the function which does not create a copy. Using the dereferencing
operator *, we can access the value stored at those addresses.

We can rewrite the above program using call by reference as well.
*/
