// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    int myArray[] = {3, 6, 9, 12, 15};
    int sixthMultiple = 18;
    int *pointer1 = &myArray[0];
    int *pointer2 = &myArray[2];
    int *pointer6 = &sixthMultiple;
    int subtracted;
    /* Valid Expression */

    subtracted = pointer2 - pointer1;
    printf("subtracted is %d\n", subtracted);

    /* Invalid Expresssion */
    subtracted = pointer2 - pointer6;
    // You do get a value here, but it is kind of garbage, it outputs 10 in this
    // case, but discerning what that means is beyond me at the moment
    printf("subtracted with sixthMultiple is %d\n", subtracted);
}

/* Outputs:
subtracted is 2
subtracted with sixthMultiple is 10
 */

/* NOTE:
Subtraction and comparison of pointers is valid only if both are members of the
same array. The subtraction of pointers gives the number of elements separating
them */
