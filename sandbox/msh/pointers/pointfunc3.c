// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>
#include <string.h>

void wish(char *p) { printf("Have a nice day, %s", p); }

int main() {
    printf("Enter your name : \n");
    char name[20];
    fgets(name, sizeof(name), stdin);
    wish(name);
}

/* Output:
Enter your name:
Brian
Have a nice day, Brian
*/
