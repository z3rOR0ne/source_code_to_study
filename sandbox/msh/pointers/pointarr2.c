// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    int marks[5][3] = {
        {98, 76, 89}, {81, 96, 79}, {88, 86, 89}, {97, 94, 99}, {92, 81, 59}};

    printf("Address of whole 2-D array = %d\n", &marks);
    printf("Addition of 1 results in %d\n", &marks + 1);

    /* Outputs
     Address of whole 2-D array = 1493727568
     Addition of 1 results in 1493727628 (incremented by 60 bytes)

     NOTATION:
     Like 1-D arrays, &marks points to the whole 2-D array, marks[5][3].
     Thus, incrementing to it by 1 ( = 5 arrays X 3 integers each X 4 bytes =
     60) results in an increment by 60 bytes.
     */

    printf("Address of 0th array = %d\n", marks);
    printf("Addition of 1 results in %d\n", marks + 1);
    printf("Address of 0th array = %d\n", &marks[0]);
    printf("Addition of 0th array = %d\n", &marks[0] + 1);

    /* Outputs
     Address of whole 2-D array = -1240250096
     Addition of 1 results in -1240250036 (incremented by 60 bytes)
     Address of 0th array = -1240250096 (same address as the whole 2d array)
     Addition of 1 results in -1240250084 (incremented by 12 bytes)
     Address of 0th array = -1240250096 (same address as the whole 2d array)
     Addition of 0th array = -1240250084 (incremented by 12 bytes)
     */

    printf("Address of 0th element of 0th array = %d\n", marks[0]);
    printf("Addition of 1 results in %d\n", marks[0] + 1);
    printf("Address of 0th element of 1st array = %d\n", marks[1]);
    printf("Addition of 1 results in %d\n", marks[1] + 1);

    /* Outputs
     Address of whole 2-D array = -1395949232 (same address as the whole 2d
     array) Addition of 1 results in -1395949172 (incremented by 60 bytes)
     Address of 0th array = -1395949232 (same address as the whole 2d array)
     Addition of 1 results in -1395949220 (incremented by 12 bytes)
     Address of 0th array = -1395949232 (same address as the whole 2d array)
     Addition of 0th array = -1395949220 (incremented by 12 bytes)
     Address of 0th element of 0th array = -1395949232 (same address as the
     whole 2d array) Addition of 1 results in -1395949228 (incremented by 4
     bytes) Address of 0th element of 1st array = -1395949220 (incremented by 12
     bytes) Addition of 1 results in -1395949216 (incremented by 4 bytes)
     */

    /* PERSONAL NOTATION:
     Obviously 2d arrays can get confusing even when just referencing their
     values, not to mention their pointers & addresses. Take your time to go
     over these concepts a few times and see the freecodecamp article this is
     based off of. Also, I have copied a table breaking down these concepts from
     the site. see pointarr.txt */
}
