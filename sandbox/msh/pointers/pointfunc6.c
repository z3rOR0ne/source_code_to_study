// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

/* Same as pointfunc5.c, but uses static keyword instead */

#include <stdio.h>

int *multiply(int *a, int *b) {
    static int c;
    c = *a * *b;
    return &c;
}

int main() {
    int a = 3, b = 5;
    int *c = multiply(&a, &b);
    printf("Product = %d\n", *c);
}

/* Output:
Product = 15
*/
