// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>
#include <stdlib.h>

int *multiply(int *a, int *b) {
    int *c = malloc(sizeof(int));
    *c = *a * *b;
    return c;
}

int main() {
    int a = 3, b = 5;
    int *(*p)(int *, int *) =
        &multiply;         /* or int* (*p)(int*, int*) = multiply; */
    int *c = (*p)(&a, &b); /* or int *c = p(&a, &b); */
    printf("Product = %d\n", *c);
}

/* Output:
Product = 15
*/

/* NOTE:
Like pointer to different data types, we also have a pointer to function as
well.

A pointer to function or function pointer stores the address of the function.
Though it doesn't point to any data. It points to the first instruction in the
function.

The syntax for declaring a pointer to function is: */

/* Declaring a function */
/* returnType functionName(parameterType1, parameterType2, ...); */

/* Declaring a pointer to a function */
/* returnType(*pointerName)(parameterType1, parameterType2, ...); */
/* pointerName = &functionName; */ /* or pointerName = functionName; */
