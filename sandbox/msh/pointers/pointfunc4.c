// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int *multiply(int *a, int *b) {
    int c = *a * *b;
    return &c;
}

int main() {
    int a = 3, b = 5;
    int *c = multiply(&a, &b);
    printf("Product = %d", *c);
}

// Doesn't work as is, see notation below:
//
/* NOTE:
The function multiply() takes two pointers to int. It returns a pointer to int
as well which stores the address where the product is stored.

It is very easy to think that the output would be 15. But it is not!

When multiply() is called, the execution of main() pauses and memory is now
allocated for the execution of multiply(). After its execution is completed, the
memory allocated to multiply() is deallocated.

Therefore, though c ( local to main()) stores the address of the product, the
data there is not guaranteed since that memory has been deallocated.

So does that mean pointers cannot be returned by a function? No !
See pointfunc5.c */
