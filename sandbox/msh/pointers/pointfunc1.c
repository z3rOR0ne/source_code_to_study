// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

void add(float *a, float *b) {
    float c = *a + *b;
    printf("Addition gives %.2f\n", c);
}

void subtract(float *a, float *b) {
    float c = *a - *b;
    printf("Subtraction gives %.2f\n", c);
}

void multiply(float *a, float *b) {
    float c = *a * *b;
    printf("Multiplication gives %.2f\n", c);
}

void divide(float *a, float *b) {
    float c = *a / *b;
    printf("Division gives %.2f\n", c);
}

int main() {
    printf("Enter two numbers:\n");
    float a, b;
    scanf("%f %f", &a, &b);

    printf(
        "What do you want to do with the numbers?\nAdd : a\nSubtract: "
        "s\nMultiply : m\nDivide : d\n");
    char operation = '0';
    scanf(" %c", &operation);

    printf("\nOperating...\n\n");

    switch (operation) {
        case 'a':
            add(&a, &b);
            break;
        case 's':
            subtract(&a, &b);
            break;
        case 'm':
            multiply(&a, &b);
            break;
        case 'd':
            divide(&a, &b);
            break;
        default:
            printf("Invalid input!!!\n");
    }
}

/* NOTE:
We created four functions, add(), subtract(), multiply() and divide() to perform
arithmetic operations on the two numbers a and b.

The address of a and b was passed to the functions. Inside the function using *
we accessed the values and printed the result. */
