// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    // Initialize a NULL pointer, do NOT leave as WILD pointer (unitialized wild
    // pointer)
    char *alphabetAddress = NULL;  // null pointer
    printf("alphabetAdress is %p\n", alphabetAddress);
    char alphabet[1] = "a";
    alphabetAddress = &alphabet[1];
    printf("alphabetAdress is now %p\n", alphabetAddress);
}

/*
Outputs:
alphabetAdress is (nil)
alphabetAdress is now 0x7ffea8ef99b8
*/
