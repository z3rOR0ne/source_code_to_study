// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    // void pointer
    void *pointer = NULL;
    int number = 54;
    char alphabet[1] = "z";
    pointer = &number;
    printf("The value of number = %d\n", *(int *)pointer);

    pointer = &alphabet;
    printf("The value of alphabet = %c\n", *(char *)pointer);
}

/*
NOTATION:
Since they are very general in nature, they are also known as generic pointers.

With their flexibility, void pointers also bring some constraints. Void pointers
cannot be dereferenced as any other pointer. Appropriate typecasting is
necessary.

Similarly, void pointers need to be typecasted for performing arithmetic
operations.

Void pointers are of great use in C. Library functions malloc() and calloc()
which dynamically allocate memory return void pointers. qsort(), an inbuilt
sorting function in C, has a function as its argument which itself takes void
pointers as its argument.
*/
