// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int multiply(int x, int y) {
    int z;
    z = x * y;
    return z;
}

int main() {
    int x = 3, y = 5;
    int product = multiply(x, y);
    printf("Product = %d\n", product);
    // prints "Product = 15"
}

/* NOTE:
The function multiply() takes two int arguments and returns their product as
int.

In the function call multiply(x,y), we passed the value of x and y ( of main()),
which are actual arguments, to multiply().*/

/* see call_by_value2.c */
