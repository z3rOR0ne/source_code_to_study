// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation
#include <stdio.h>

int main() {
    /* Array of Strings */
    char top[6][15] = {"Liverpool", "Man City",  "Man United",
                       "Chelsea",   "Leicester", "Tottenham"};

    printf("Pointer to 2-D array = %d\n", &top);
    printf("Addition of 1 results in %d\n", &top + 1);

    /* Outputs
    Pointer to 2-D array = -897515520
    Addition of 1 results in -897515430 (increments by 90 bytes)
    */

    printf("Pointer to 0th string =%d\n", &top[0]);
    printf("Addition of 1 results in %d\n", &top[0] + 1);

    /* Outputs
    Pointer to 0th string =-1221528064
    Addition of 1 results in -1221528049 (increments by 25 bytes)
    */

    printf("Pointer to 0th string = %d\n", top);
    printf("Addition of 1 results in %d\n", top + 1);

    /* Outputs
    Pointer to 0th string = 1924432512
    Addition of 1 results in 1924432527 (increments by 15 bytes)
    */

    printf("Pointer to 0th element of 4th string = %d\n", top[4]);
    printf("Pointer to 1st element of 4th string = %d\n", top[4] + 1);

    /* Outputs:
Pointer to 0th element of 4th string = -1859412756
Pointer to 1st element of 4th string = -1859412755 (increments by 1 byte)
    */

    printf("Value of 1st character in 3rd string = %c\n", top[3][1]);
    printf("Same using pointers = %c\n", *(*top + 3) + 1);

    /* Outputs
    Value of 1st character in 3rd string = h
    // UNEXPECTED BEHAVIOR, it was supposed to be an h...
    Same using pointers = f
    */
}
