// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>
#include <stdlib.h>

/* Using malloc() */

int *multiply(int *a, int *b) {
    int *c = malloc(sizeof(int));
    *c = *a * *b;
    return c;
}

int main() {
    int a = 3, b = 5;
    int *c = multiply(&a, &b);
    printf("Product = %d\n", *c);
}

/* Outputs:
Product = 15
*/

/* NOTE:
We can do two things. Either store the address in the heap or global section or
declare the variable to be static so that their values persist.

Static variables can simply be created by using the keyword static before data
type while declaring the variable.

To store addresses in heap, we can use library functions malloc() and calloc()
which allocate memory dynamically.

The following programs will explain both the methods. Both methods return the
output as 15. See pointfunc6.c */
