// Taken from https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

6. Pointer to Array

Like "pointer to int" or "pointer to char", we have pointer to array as well. This pointer points to whole array rather than its elements.

Remember we discussed how &arrayName points to the whole array? Well, it is a pointer to array.

A pointer to array can be declared like this:

dataType (*variableName)[size];

/* Examples */
int (*ptr1)[5];
char (*ptr2)[15];

Notice the parentheses. Without them, these would be an array of pointers. The first example can be read as - ptr1 is a pointer to an array of 5 int(integers).

see pointer_to_array.c
