// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h> #include <stdlib.h>  // for malloc and free

int main() {
    // Dangling Pointer
    int *ptr;
    ptr = (int *)malloc(sizeof(int));
    *ptr = 1;
    printf("The value of ptr is %d\n", *ptr); /* prints 1 */
    printf("The address of ptr is %p\n", &ptr);

    free(ptr); /* deallocation */
    *ptr = 5;
    printf("The new value of ptr is %d\n", *ptr);   /* may or may not print 5 */
    printf("The new address of ptr is %p\n", &ptr); /* may or may not print 5 */
}

/*
Outputs:
The value of ptr is 1
The address of ptr is 0x7ffd6b8e4870
The new value of ptr is 5
The new address of ptr is 0x7ffd6b8e4870
*/

/*
NOTATION:
A dangling pointer points to a memory address which used to hold a variable.
Since the address it points at is no longer reserved, using it will lead to
unexpected results.

Though the memory has been deallocated by free(ptr), the pointer to integer ptr
still points to that unreserved memory address.
 */
