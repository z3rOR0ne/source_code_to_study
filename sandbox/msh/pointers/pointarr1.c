// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    int prime[5] = {2, 3, 5, 7, 11};
    // of course you can hard code the length of an array,
    // but this at least has syntax similar to higher level languages(used in
    // for loop below): taken from: https://flaviocopes.com/c-array-length/
    const int arrlen = sizeof prime / sizeof prime[0];
    // alternatively you could also use:
    /* const int arrlen = sizeof prices / sizeof *prime; */

    printf("These are the initial memory addresses:\n");
    printf("Result using &prime = %d\n", &prime);
    printf("Result using prime = %d\n", prime);
    printf("Result using &prime[0] = %d\n", &prime[0]);

    /* Outputs: // demonstrating they all are assigned to the same memory
    address These are the initial memory addresses: Result using &prime =
    1940717984 Result using prime = 1940717984 Result using &prime[0] =
    1940717984
    */

    printf("But what happens to the memory addresses if we increment by 1?\n");
    printf("Result using &prime = %d\n", &prime + 1);
    printf("Result using prime = %d\n", prime + 1);
    printf("Result using &prime[0] = %d\n", &prime[0] + 1);

    /* Outputs:
    But what happens to the memory addresses if we increment by 1?
    Result using &prime = 1940718004
    Result using prime = 1940717988
    Result using &prime[0] = 1940717988

    PERSONAL NOTATION/EXPLANATION:
    Basically the first element &prime points to the whole array and when we
    increase it by one, we change the address of the whole array, but NOT its
    elements. The address is increased by the size of int multiplied by the size
    of the array (5x4), thusly when you see the original value of prime it is
    194071798, but after the addition of 1 to the value of the &prime, it is
    increased by 20 bytes to 1940718004.

    The other two, prime and &prime[0], both reference the first element of the
    array, and so their memory addresses are increased by adding 4 bits
    (194071798 to 194071798)
*/

    // We can access the array elements using the subscripted variables in a for
    // loop:

    for (int i = 0; i < arrlen; i++)
        printf("index = %d, address = %d, value = %d\n", i, &prime, prime[i]);

    /* Outputs
    index = 0, address = 148577664, value = 2
    index = 1, address = 148577664, value = 3
    index = 2, address = 148577664, value = 5
    index = 3, address = 148577664, value = 7
    index = 4, address = 148577664, value = 11
*/
}
