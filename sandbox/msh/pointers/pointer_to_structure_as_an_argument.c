// Taken from https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

struct records {
    char name[20];
    int roll;
    int marks[5];
    char gender;
};
// different from tutorial, order matters here
void
printRecords(struct records *ptr){
   printf("Name: %s\n", ptr->name);
   printf("Roll: %d\n", ptr->roll);
   printf("Gender: %c\n", ptr->gender);
   for(int i = 0; i < 5; i++)
       printf("Marks in %dth subject: %d\n", i, ptr->marks[i]);
}

int
main()
{
   struct records students = {"Alex", 43, {76, 98, 68, 87, 93}, 'M'};
   printRecords(&students);
}


/* Output:
Name: Alex
Roll: 43
Gender: M
Marks in 0th subject: 76
Marks in 1th subject: 98
Marks in 2th subject: 68
Marks in 3th subject: 87
Marks in 4th subject: 93
 */
