// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    int goals[] = {85, 102, 66, 69, 67};
    int(*pointerToGoals)[5] = &goals;

    printf("Address stored in pointerToGoals %d\n", pointerToGoals);
    printf("Dereferencing it, we get %d\n", *pointerToGoals);

    /* Outputs
    Address stored in pointerToGoals 1486442144
    Dereferencing it, we get 1486442144 (same address)
     */

    printf("Size of goals[5] = %d\n", sizeof(*pointerToGoals));
    /* Outputs
    Size of goals[5] = 20
    */

    for (int i = 0; i < 5; i++) printf("%d ", *(*pointerToGoals + i));

    /* Outputs
    85 102 66 69 67
    */
}
