// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    /* String */
    char champions[] = "Liverpool";

    printf("Pointer to whole string = %d\n", &champions);
    printf("Addition of 1 results in %d\n", &champions + 1);

    /* Outputs:
    Pointer to whole string = 902708750
    Addition of 1 results in 902708760 (increments by 10 bytes)
    */

    printf("Pointer to 0th character = %d\n", &champions[0]);
    printf("Addition of 1 results in %d\n", &champions[0] + 1);

    /* Outputs
    Pointer to 0th character = -1849390722
    Addition of 1 results in -1849390721 (increments by 1 byte)
     */

    printf("Pointer to the 0th character = %d\n", champions);
    printf("Addition of 1 results in a pointer to 1st character %d\n",
           champions + 1);

    /* Outputs
    Pointer to the 0th character = 1059698174
    Addition of 1 results in a pointer to 1st character 1059698175 (increments
    by 1 byte)
    */

    printf("Value of 4th character = %c\n", champions[4]);
    printf("Value of 4th character using pointers = %c\n", *(champions + 4));

    /* Outputs
    Value of 4th character = r
    Value of 4th character using pointers = r
    */

    /* NOTATION
    A string is a one-dimensional array of characters terminated by a null(\0).
    When we write char name[] = "Srijan";, each character occupies one byte of
    memory with the last one always being \0.

    Similar to the arrays we have seen, name and &name[0] points to the 0th
    character in the string, while &name points to the whole string. Also,
    name[i] can be written as *(name + i).
    */
}
