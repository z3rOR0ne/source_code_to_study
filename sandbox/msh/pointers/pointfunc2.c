// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

void greatestOfAll(int *p) {
    int max = *p;
    for (int i = 0; i < 5; i++) {
        if (*(p + i) > max) max = *(p + i);
    }
    printf("The largest element is %d\n", max);
}

int main() {
    int myNumbers[5] = {34, 65, -456, 0, 3455};
    greatestOfAll(myNumbers);
    /* Output:
    The largest element is 3455
     */
}

/* NOTE: Since the name of an array itself is a pointer to the first element, we
 * send that as an argument to the function greatestOfAll(). In the function, we
 * traverse through the array using loop and pointer. */
