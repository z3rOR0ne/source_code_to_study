// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

/*
Like any other pointer, function pointers can also be passed to another
function, therefore known as a callback function or called function. The
function to which it is passed is known as a calling function.

A better way to understand would be to look at qsort(), which is an inbuilt
function in C. It is used to sort an array of integers, strings, structures, and
so on. The declaration for qsort() is: */

/* void qsort(void *base, size_t nitems, size_t size, int (*compar)(const void
 * *, const void*)); */

/*qsort() takes four arguments:

    a void pointer to the start of an array
    number of elements
    size of each element
    a function pointer that takes in two void pointers as arguments and returns
an int

The function pointer points to a comparison function that returns an integer
that is greater than, equal to, or less than zero if the first argument is
respectively greater than, equal to, or less than the second argument.

The following program showcases its usage: */

#include <stdio.h>
#include <stdlib.h>

int compareIntegers(const void *a, const void *b) {
    const int *x = a;
    const int *y = b;
    return *x - *y;
}

int main() {
    int myArray[] = {97, 59, 2, 83, 19, 97};
    int numberOfElements = sizeof(myArray) / sizeof(int);

    // The following two lines aren't part of the tutorial, just re-emphasizing
    // how pointers to arrays work.
    /* printf("The first element is: %d\n", *myArray); // outputs 97 */
    /* printf("The first element is: %d\n", myArray[0]); // outputs 97 */

    printf("Before sorting - \n");
    for (int i = 0; i < numberOfElements; i++) printf("%d ", *(myArray + i));

    qsort(myArray, numberOfElements, sizeof(int), compareIntegers);

    printf("\n\nAfter sorting - \n");
    for (int i = 0; i < numberOfElements; i++) printf("%d ", *(myArray + i));
    printf("\n");
}

/* Output
Before sorting -
97 59 2 83 19 97

After sorting -
2 19 59 83 97 97

*/
