// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int multiplye(int x, int y) {
    printf("Address of x in multiply() = %d\n", &x);
    printf("Address of y in multiply() = %d\n", &y);

    int z;
    z = x * y;
    return z;
}

int main() {
    int x = 3, y = 5;
    printf("Address of x in main() = %d\n", &x);
    printf("Address of y in main() = %d\n", &y);
    int product = multiplye(x, y);
    printf("Product = %d\n", product);

    /* Output
    Address of x in main() = 177532524
    Address of y in main() = 177532528
    Address of x in multiply() = 177532476
    Address of y in multiply() = 177532472
    Product = 15
     */
}

/*NOTE:
The values of the actual arguments are passed or copied to the formal arguments
x and y ( of multiply()). The x and y of multiply() are different from those of
main(). This can be verified by printing their addresses.*/

/* see call_by_value3.c */
