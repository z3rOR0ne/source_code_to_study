// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    struct records {
        char name[20];
        int roll;
        int marks[5];
        char gender;
    };

    struct records student = {"Alex", 43, {76, 98, 68, 87, 93}, 'M'};

    struct records *ptrStudent = &student;

    printf("Address of structure = %d\n", ptrStudent);
    printf("Address of member `name` = %d\n", &student.name);
    printf("Increment by 1 results in %d\n", ptrStudent + 1);

    /*Output:
    Address of structure = 703313104 // address of the whole structure
    Address of member `name` = 703313104 // which is equal to the address of its
    first element Increment by 1 results in 703313152 // adding 1 to the whole
    structure, the address is increased by 48 bytes
     */

    printf("Name of w.o using ptrStudent : %s\n", student.name);
    printf("Name using ptrStudent and * : %s\n", (*ptrStudent).name);
    printf("Name using ptrStudent and -> : %s\n", ptrStudent->name);

    /* Output
    Name of w.o using ptrStudent : Alex
    Name using ptrStudent and * : Alex
    Name using ptrStudent and -> : Alex
     */
}

/* NOTE ABOUT DEREFERENCING, taken from stackoverflow:
https://stackoverflow.com/questions/18251340/what-does-this-mean
 *
The -> operator is shorthand for deferencing and then accessing a member.

Given

type *a;

(*a).b is equivalent to a->b */
