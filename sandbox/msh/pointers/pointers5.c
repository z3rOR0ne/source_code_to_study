// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation
#include <stdio.h>

int main() {
    // Pointer Arithmetic
    int myArray[6] = {3, 6, 9, 12, 15};
    int *pointerToMyArray = &myArray[0];
    printf("The size of pointerToMyArray is %lu\n", sizeof(*pointerToMyArray));
    printf("The value of myArray[0] is %d\n", myArray[0]);
    printf("The value of *pointerToMyArray is %d\n", *pointerToMyArray);

    pointerToMyArray += 3;
    /* *pointerToMyArray += 3; if you actually want to add it and get 6*/
    printf("My new value of pointerToMyArray is %d\n",
           *pointerToMyArray);  // outputs 12
}

/*
Outputs:
The size of pointerToMyArray is 4
The value of myArray[0] is 3
The value of *pointerToMyArray is 3
My new value of pointerToMyArray is 12
*/

/*
NOTATION:
When you add (or subtract) an integer (say n) to a pointer, you are not actually
adding (or subtracting) n bytes to the pointer value. You are actually adding
(or subtracting) n-times the size of the data type of the variable being pointed
bytes (this is why we get 12 above and not 6, the sizeof(*pointerToMyArray) is 4
and the amount we added to pointerToMyArray is 3, thusly resulting in 12).
*/
