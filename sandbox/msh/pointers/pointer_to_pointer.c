// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

/* So far we have looked at pointer to various primitive data types, arrays,
strings, functions, structures, and unions.

The automatic question that comes to the mind is – what about pointer to
pointer?

Well, good news for you! They too exist. */

#include <stdio.h>

int main() {
    int var = 6;
    int *ptr_var = &var;

    printf("Address of var = %d\n", ptr_var);
    printf("Address of ptr_var = %d\n", &ptr_var);

    /* Output:
        Address of var = -914187652
        Address of ptr_var = -914187648 // an increase of 4 bytes
    */
    /* To store the address of int variable var, we have the pointer to int
    ptr_var. We would need another pointer to store the address of ptr_var.

    Since ptr_var is of type int *, to store its address we would have to create
    a pointer to int *. The code below shows how this can be done. */

    int **ptr_ptrvar = &ptr_var; /* or int* *ppvar or int **ppvar */

    /* We can use ptr_ptrvar to access the addresss of ptr_var and use double
     * dereferencing to access var. */
    printf("Address of ptr_var = %d\n", ptr_ptrvar);
    printf("Address of var = %d\n", *ptr_ptrvar);
    printf("Value at var = %d\n", *(*ptr_ptrvar));

    /* Output:
     Address of ptr_var = 1683523336
     Address of var = 1683523332 // a subtraction of 4 bytes
     Value at var = 6
     */

    /* It is not required to use brackets when dereferencing ptr_ptrvar. But it
    is a good practice to use them. We can create another pointer ptr_ptrptrvar,
    which will store the address of ptr_ptrvar.

    Since ptr_ptrvar is of type int**, the declaration for ptr_ptrptrvar will be
  */

    int ***ptr_ptrptrvar = &ptr_ptrvar;

    /* We can againa ccess ptr_ptrvar, ptr_var and var using ptr_ptrptrvar */

    printf("Address of ptr_ptrvar = %d\n", ptr_ptrptrvar);
    printf("Value at ptr_ptrvar = %d\n", *ptr_ptrptrvar);
    printf("Address of ptr_var = %d\n", *ptr_ptrptrvar);
    printf("Value at ptr_var = %d\n", *(*ptr_ptrptrvar));
    printf("Address of var = %d\n", *(*ptr_ptrptrvar));
    printf("Value at var = %d\n", *(*(*ptr_ptrptrvar)));

    /* Output:
 Address of ptr_ptrvar = -1612082568
 Value at ptr_ptrvar = -1612082576
 Address of ptr_var = -1612082576
 Value at ptr_var = -1612082580
 Address of var = -1612082580
 Value at var = 6
     */

    /* If we change the value at any of the pointer(s) using ptr_ptrptrvar or
     * ptr_ptrvar, the pointer(s) will stop pointing to the variable. */
}
