// Taken from
// https://www.freecodecamp.org/news/pointers-in-c-are-not-as-difficult-as-you-think/#2-definition-and-notation

#include <stdio.h>

int main() {
    int ManU = 1;
    int *addressOfManU = &ManU;
    printf("Adress of Manu: %p\n", &addressOfManU);
    // null pointer
    int *anotherAddressOfManU = NULL;
    anotherAddressOfManU = addressOfManU; /* Valid */
    printf("Another address of Manu: %p\n", &anotherAddressOfManU);
    /* double *wrongAddressofManu = addressOfManu; [> Invalid <] */
}

/*
Outputs:
Adress of Manu: 0x7ffece3b3f68
Another address of Manu: 0x7ffece3b3f70
*/

/*
NOTATION:
You can assign the value of one pointer to another only if they are of the same
type (unless they're typecasted or one of them is void *).
*/
