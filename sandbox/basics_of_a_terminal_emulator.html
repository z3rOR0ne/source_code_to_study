<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        
    <link rel="alternate" href="https://www.uninformativ.de/blog/feeds/en.atom"
        title="Blog Feed (en)" type="application/atom+xml"/>
    <link rel="alternate" href="https://www.uninformativ.de/blog/feeds/de.atom"
        title="Blog Feed (de)" type="application/atom+xml"/>
        <link rel="stylesheet" href="../../../../simplicity.css">
        <title>The very basics of a terminal emulator</title>
    </head>
    <body>
        <div id="blogpostcontent">

<p><a href="/blog/">blog</a> · <a href="/git/">git</a> · <a href="/desktop/">desktop</a> · <a href="/pics/stream.html">images</a> · <a href="/contact.html">contact &amp; privacy</a> · <a href="gopher://uninformativ.de">gopher</a></p>
<hr />
<h1>The very basics of a terminal emulator</h1>
<p class="blogpostdate">2018-02-24</p>

<p><a href="http://invisible-island.net/xterm/xterm.html">XTerm</a>, <a href="https://st.suckless.org/">st</a>, <a href="https://github.com/GNOME/vte">libvte</a>, and others. Terminal emulators. Pretty much
anyone uses them – some people all day long. The big question remains,
what do they do, how do they work?</p>
<p>Let’s write a very basic terminal emulator. This has been on my TODO
list for a long time. You can find my example program over here:</p>
<p><a href="/git/eduterm">eduterm</a></p>
<p><a href="eduterm.png"><img alt="eduterm" src="eduterm.png" /></a></p>
<h2>Where to start?</h2>
<p>Let’s assume we’re on Linux. You have probably run the “<code>w</code>” command at
some point. Its output looks like this:</p>
<pre><code>$ w
 16:43:05 up  8:24,  2 users,  load average: 0.58, 1.06, 1.26
USER     TTY        LOGIN@   IDLE   JCPU   PCPU WHAT
void     tty1      09:31    7:11m 14:12   0.00s xinit /home/...
void     pts/10    16:42    0.00s  0.02s  0.00s w
$
</code></pre>
<p>“<code>tty1</code>” is the Linux text console where I logged in. The second line
shows “<code>pts/10</code>” and this is the XTerm that I’m running “<code>w</code>” in. (It
doesn’t show all the other terminals that are currently open because VTE
doesn’t update <a href="https://en.wikipedia.org/wiki/Utmp">utmp</a> anymore.)</p>
<p>The term “pts” points us into the right direction. It even has a
manpage:</p>
<pre><code>$ man pts
</code></pre>
<p>That manpage tells you almost everything you need to know. In
particular:</p>
<ul>
<li>We’re talking about “pseudoterminals”. “Pseudo” because we won’t be
    dealing with actual <a href="https://en.wikipedia.org/wiki/Computer_terminal">terminal hardware</a>.</li>
<li>There is a master and a slave file descriptor.</li>
<li>The master will be used by your emulator and the slave by the
    program running “inside” of your emulator.</li>
</ul>
<p>How do you get a master slave pair? That’s a bit tricky. The BSDs have
“<code>openpty()</code>” which does all the work for you. suckless st uses this.
The manpage says that this function is “not standardized in POSIX”, so,
yeah, maybe you shouldn’t use it? Maybe it’s fine? Nobody really knows.
Even <a href="https://git.musl-libc.org/cgit/musl/tree/src/misc/openpty.c">musl libc</a> has this function (and note the comment on this
function being “vastly superior”).</p>
<p>For my little example program, I went with “<code>posix_openpt()</code>”. It has a
different interface and requires you to do more work. It goes like this:</p>
<ul>
<li><code>posix_openpt()</code> returns a file descriptor to a master device.</li>
<li><code>grantpt(master)</code> adjusts the file system permissions of the
    corresponding slave device, such that you will be able to open that
    device.</li>
<li><code>unlockpt(master)</code> “unlocks” your slave. This basically means that
    you are now allowed to actually open the slave device.</li>
</ul>
<p>I’m not 100% sure why we need this little dance. The interface of
“<code>openpty()</code>” feels a lot better.</p>
<p>Okay then. Run these three calls and you have a pair of master and slave
file descriptors. It’s done in the “<code>pt_pair()</code>” function in my example
program.</p>
<h2>Connecting a child process to your terminal</h2>
<p>You basically have to do this:</p>
<pre><code>p = fork();
if (p == 0)
{
    close(pty-&gt;master);

    setsid();
    if (ioctl(pty-&gt;slave, TIOCSCTTY, NULL) == -1)
    {
        perror("ioctl(TIOCSCTTY)");
        return false;
    }

    dup2(pty-&gt;slave, 0);
    dup2(pty-&gt;slave, 1);
    dup2(pty-&gt;slave, 2);
    close(pty-&gt;slave);

    execle("/bin/sh", "/bin/sh", NULL, env);
}
</code></pre>
<p>You do a fork to get a new process that inherits virtually all
properties of the parent. In our child, we close the master file
descriptor. The child will operate only with the slave, so we “copy” the
slave file descriptor to stdin, stdout and stderr. This allows the shell
to talk to us.</p>
<p>We also create a new session group using <code>setsid()</code>, which implicitly
creates a new process group as well. These two concepts have become
somewhat “arcane” these days. It’s explained over here:</p>
<p><a href="https://www.win.tue.nl/~aeb/linux/lk/lk-10.html">https://www.win.tue.nl/~aeb/linux/lk/lk-10.html</a></p>
<p>And it looks like this (I hope this helps):</p>
<pre><code>                     sh
+-------------------------------------------+
|                                           |
| +---------------+  +-------+   +--------+ |
| |+----+ +------+|  |+-----+|   |+------+| |
| || ls | | grep ||  || vim ||   || find || |
| |+----+ +------+|  |+-----+|   |+------+| |
| +---------------+  +-------+   +--------+ |
|                                           |
+-------------------------------------------+
</code></pre>
<p><code>ls</code>, <code>grep</code>, <code>vim</code>, and <code>find</code> are all individual processes. <code>ls</code> and
<code>grep</code> are put together in a process group because let’s assume you ran
“<code>ls | grep foo</code>”. <code>vim</code> and <code>find</code> are in their own process group. All
of them, including <code>sh</code>, are in a session and <code>sh</code> is the leader of that
session.</p>
<p>Let’s assume that “<code>ls | grep foo</code>” is the foreground process group.
This means you have started <code>vim</code> and <code>find</code> earlier and put both of
them “in the background” using ^Z. When you now hit ^C, SIGINT will be
delived to the foreground process group, i.e. our pipe.</p>
<p>You can inspect these groups using <code>ps</code> (which does not show session IDs
per default):</p>
<pre><code>$ ps -o pid,pgid,sess,comm
  PID  PGID  SESS COMMAND
 3301  3301  3301 bash
22474 22474  3301 vim
22547 22547  3301 find
23059 23059  3301 bash
23060 23059  3301 cat
23126 23059  3301 sleep
23291 23291  3301 ps
</code></pre>
<p>Some of these processes share a process group ID and they all share the
same session ID. PID and PGID are the same for most processes, which
means that they are also the process group leader – so, <code>vim</code>, <code>find</code>,
and <code>ps</code> are the only processes in their respective process groups.
Both <code>cat</code> and <code>sleep</code> have the same PGID: I ran “<code>while sleep 1; do
date; done | cat</code>” and then hit ^Z. That made the shell fork (bash with
PID 23059 becomes the process group leader) and then fork again a few
times to exec new programs.</p>
<p>Okay, enough of that. Back to the code.</p>
<p>We have made our terminal the controlling terminal for the entire
session using the <code>ioctl(pty-&gt;slave, TIOCSCTTY, NULL)</code> call. This is
probably the most interesting part in this chapter because it informs
the kernel that a SIGINT shall be delivered to the foreground process
group when you hit ^C in your terminal emulator.</p>
<h2>Pseudoterminals do a lot of the work</h2>
<p>Okay, we now have a child process running and it’s connected to our
slave device. You can now write to your master device and it will be
delivered as input to your child.</p>
<p>This means that the other part of your terminal emulator can be a
regular X11 client. All you have to do is convert X11 key events to
“bytes” and write them to the master device. In X11 land, you get an
event and a <code>XKeyEvent</code> struct with a keycode field and other fields
indicating modifiers like “Shift”. You have to figure out which ASCII
character corresponds to this struct. Luckily, <code>XLookupString()</code> is good
enough for my very simple example.</p>
<p>And, of course, you have to read from the master device and display any
characters in an X11 window.</p>
<pre><code>+------------+      +================+      +----------------------+
| X11 client | &lt;--&gt; | pseudoterminal | &lt;--&gt; | terminal application |
+------------+      +================+      +----------------------+
</code></pre>
<p>The “X11 client” is your terminal emulator. It doesn’t have to be an X11
client, but let’s not get distracted.</p>
<p>The “pseudoterminal” thingy is <em>not</em> part of your code. It’s a driver
that lives in the kernel. And it performs a surprisingly large part of
the job.</p>
<p>For example, you don’t have to deal with signals. You literally write a
“^C” (a byte with value 0x03) to your master device and that’s it. You
don’t have to keep track of “foreground process groups” and what not.</p>
<p>You don’t even have to implement “terminal modes”. Have you ever used
“<code>read -s</code>” in a shell script? Try it:</p>
<pre><code>read -sp 'Type something and hit Enter: '
echo "You said: '$REPLY'"
</code></pre>
<p>You will notice that you don’t see what you type. The shell (or any
other program) accomplishes this by running <code>ioctl(stdin, TCSETSW, ...)</code>
to instruct “the terminal” to not display any character entered by the
user. But as it turns out, this is handled by the pseudoterminal driver
and not the actual terminal emulator.</p>
<p>At the end of the day, a terminal emulator like XTerm or my example
program is nothing but a converter from X11 events to simple bytes.</p>
<h2>Okay, what does your terminal emulator have to emulate then?</h2>
<p>Except that it isn’t. I lied.</p>
<p>There are some things that you don’t have to implement. On the other
hand, the pseudoterminal driver is ignorant of any escape sequences. And
this is where the fun starts.</p>
<pre><code>printf '\033[1mThis is bold text.\n'
</code></pre>
<p>If you run this in my example program, you won’t see bold text. You
won’t see any kind of color. You won’t be able to run curses
applications. Not even <code>vi</code> works (but <code>ed</code> does, so you should be fine
:-)).</p>
<p>It’s all those escape sequences that you have to interpret. Are you
compatible with a <a href="https://en.wikipedia.org/wiki/VT100">VT100</a>? Do you understand the “non-standard” escape
sequences of XTerm that allow you to use 256 colors? Do you support a
secondary screen buffer or special line drawing characters?</p>
<p>If your user presses “cursor key up”, which bytes do you write to your
master device? ^C was easy because it has been mapped to 0x03 a long
time ago. For cursor keys, there is no such mapping. Ever seen a stray
<code>^[[A</code> in your terminal? That might have been the escape sequence
generated by your terminal emulator and written to its master device to
indicate “cursor up” – the terminal application has to read and
interpret this sequence.</p>
<p>Oh and speaking of “sequences”: Since UTF-8 is a multibyte encoding,
have fun dealing with that. :-) My simple example program ignores all
that and just expects ASCII.</p>
<p>Even plain ASCII can be challenging (or “annoying”). There’s the newline
character. When you read it, you have to move the cursor one line
downwards. Now, what do you do when your terminal is 80 cells wide and
you read “a” 80 times followed by a newline? The 80th “a” has pushed
your cursor to the 81st column which does not exist, so you probably
wrap to the next line. Do you then ignore the following newline? Do you
process it and create two line feeds?</p>
<p>Even simple stuff like that can cause a minor headache. Let alone
atrocities like this:</p>
<pre><code>printf 'ä\033[120b'
</code></pre>
<p>This prints an “a-umlaut” (UTF-8!) and then prints an escape sequence
which instructs the terminal to repeat the previous character 120 times.</p>
<h2>Conclusion</h2>
<p>The basics of a terminal emulator are relatively easy to understand. You
can tell by the short Git history of my program that it didn’t take long
to write. I still learned a great deal.</p>
<p>I’ll stop at this point. Maybe I’ll implement some more interesting
features, but I will not write a new terminal emulator from scratch any
time soon. There’s a reason it’s called “curses”. And reinventing the
wheel really doesn’t help this time – if you want to hack on a terminal,
go hack on <a href="https://st.suckless.org/">st</a>.</p>
<p>It could be interesting, though, to look at the code that implements
pseudoterminals. And/or to have a look at the BSD details.</p>
<p>I wonder what real hardware terminals were like. I assume that
“pseudoterminals” didn’t exist back then. This means that an actual
VT100 had to implement stuff like “cooked mode”, “echo mode”, and all
that by itself. Right?</p>
<p class="blogpostcomments"><a href="/contact.html">Comments?</a></p>

        </div>
    </body>
</html>

