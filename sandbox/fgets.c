/* https://www.tutorialspoint.com/c_standard_library/c_function_fgets.htm */
#include <stdio.h>

int
main()
{
    FILE *fp;
    char str[60];

    /* opening file for reading */
    fp = fopen("file.txt", "r");
    if(fp == NULL) {
        perror("Error opening file");
        return(-1);
    }
    if(fgets(str, 60, fp) != NULL) {
        /* writing content to stdout */
        puts(str);
    }
    fclose(fp);

    return(0);
}

// If we have a file.txt file, it will output whatever text is within that file.
